#!/usr/bin/env bash

# colors
G="\033[0;32m"
R="\033[0;31m"
N="\033[0m"

# global precondition check state
ERROR="false"

function showUsage() {
  echo "usage: publishMavenCentral.sh"
  echo "                    -t <target_tag>"
  echo "                    [-d]"
  echo "                    [-h]"
  echo "       -h: Shows this help message."
  echo "       -d: dry run, do all but gradle publish"
  echo "       <target_tag>: the tag that should be published,"
  echo "                     e.g. 1.0.0"
  echo "       <restore_branch>: the local branch that should be "
  echo "                         checked out after release"
}

function parseArgs() {
  TARGET_TAG=""
  DRY_RUN=""
  HELP="false"
  for p in "$@"; do
    case "$p" in
      -t)
      shift # past argument
      TARGET_TAG="$1"
      ;;
      -d)
      shift # past argument
      DRY_RUN="-d"
      ;;
      -h)
      shift # past argument
      HELP="true"
      ;;
      *)
      shift
      ;;
    esac
  done
  if [[ -z "$TARGET_TAG" ]]; then
    echo "$TARGET_TAG"
    echo "Target tag is mandatory." >&2
    showUsage
    exit 1
  fi
  if [[ "$HELP" == "true" ]]; then
    showUsage
    exit 0
  fi
}

function ok() {
  echo -e "${G}Ok${N}"
}

function fail() {
  ERROR="true"
  echo -e "${R}Fail${N} $1"
}

function failNow() {
  fail "$1"
  exit 1
}

function check() {
  echo "Checking preconditions"

  echo -n "* git installed:                                   "
  which -s git \
    && ok || failNow ""

  echo -n "* no local changes:                                "
  test $(git status -s | wc -l) == "0" \
    && ok || fail "check git status"

  PROPERTIES=~/.gradle/gradle.properties

  echo -n "* gradle.properties exist:                         "
  test -f "$PROPERTIES" \
    && ok || failNow "create $PROPERTIES"

  echo -n "* signing key configured for gradle:               "
  KEY_ID=$(cat "$PROPERTIES" | grep "signing.keyId=" | awk '{sub(/.*=/, ""); print}')
  test -n "$KEY_ID" \
    && ok || fail "set signing.keyId in $PROPERTIES"

  echo -n "* signing key exists:                              "
  [[ $(gpg --list-keys "$KEY_ID" 2>/dev/null) ]] \
    && ok || fail "no key with id $KEY_ID"

  echo -n "* signing password configured for gradle:          "
  test -n $(cat "$PROPERTIES" | grep "signing.password=" | awk '{sub(/.*=/, ""); print}') \
    && ok || fail "set signing.password in $PROPERTIES"

  echo -n "* signing secretKeyRingFile configured for gradle: "
  test -n $(cat "$PROPERTIES" | grep "signing.secretKeyRingFile=" | awk '{sub(/.*=/, ""); print}') \
    && ok || fail "set signing.secretKeyRingFile in $PROPERTIES"

  echo -n "* sonatypeUsername configured for gradle:          "
  test -n $(cat "$PROPERTIES" | grep "sonatypeUsername=" | awk '{sub(/.*=/, ""); print}') \
    && ok || fail "set sonatypeUsername in $PROPERTIES"

  echo -n "* sonatypeUsername is not placeholder:             "
  test -z $(cat "$PROPERTIES" | grep "sonatypeUsername=<YOUR_SONATYPE_USERNAME>") \
    && ok || fail "set sonatypeUsername in $PROPERTIES"

  echo -n "* sonatypePassword configured for gradle:          "
  test -n $(cat "$PROPERTIES" | grep "sonatypePassword=" | awk '{sub(/.*=/, ""); print}') \
    && ok || fail "set sonatypePassword in $PROPERTIES"

  echo -n "* sonatypePassword is not placeholder:             "
  test -z $(cat "$PROPERTIES" | grep "sonatypePassword=<YOUR_SONATYPE_PASSWORD>") \
    && ok || fail "set sonatypePassword in $PROPERTIES"

  echo -n "* using Java 8:                                    "
  java -version 2>&1 | grep "build 1.8.0_" > /dev/null \
    && ok || fail ""

  [[ "$ERROR" == "true" ]] && exit 1
}

parseArgs "$@"

check

echo ""
echo "Preparing release"

git fetch -q && git fetch --tags -q
BRANCH=`git branch | grep -e "^* " | awk '{sub(/\* /, ""); print}'`
TAG=`git status | grep "HEAD detached at " | awk '{sub(/^HEAD detached at /, ""); print}'`

if [[ -z "$TAG" ]]; then
  echo -n "Need to checkout the release tag $TARGET_TAG ... "
  git checkout "$TARGET_TAG" -q
  echo -e "${G}done${N}"
  echo ""
  echo "Retrying to publish"
  echo ""
  [[ -f "publishMavenCentral.sh" ]] \
    && bash publishMavenCentral.sh -t "$TARGET_TAG" -b "$BRANCH" "$DRY_RUN" \
    || fail "You are trying to publish a tag that does not have this script."
  [[ -n "$BRANCH" ]] && git checkout "$BRANCH" -q
else
  [[ "$TAG" != "$TARGET_TAG" ]] && fail "Checked out $TAG but $TARGET_TAG should be published"
  echo "Executing release"
  if [[ "$DRY_RUN" == "-d" ]]; then
    echo "DRY: gradlew clean assemble publishSonatype \"-Pversion=${TARGET_TAG}\""
  else
    bash gradlew clean assemble publishSonatype "-Pversion=${TARGET_TAG}"
    echo "Close and release the staging repository at https://oss.sonatype.org/#stagingRepositories"
  fi
fi
