package org.codefetti.proxy.handler;

import org.junit.Test;

import java.lang.reflect.InvocationHandler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.codefetti.proxy.handler.InterfaceProxyBuilder.createProxy;

@SuppressWarnings("SuspiciousInvocationHandlerImplementation")
public class ProxyTest {

    private final InvocationHandler invocationHandler = (proxy, method, args) -> {
        if ("getValue".equals(method.getName()) && method.getParameterCount() == 1 && String.class
                .equals(method.getParameterTypes()[0])) {
            return "getValue received argument: " + args[0];
        }
        return null;
    };

    private final AnInterface proxy = createProxy(AnInterface.class, invocationHandler);

    @Test
    public void shouldInvokeRealDefaultMethod() {
        String actual = proxy.getDefaultValue();
        assertThat(actual).isEqualTo("getValue received argument: default");
    }

    @Test
    public void shouldInvokeRealDefaultMethodMultipleTimes() {
        for (int i = 0; i < 100; i++) {
            String actual = proxy.getDefaultValue();
            assertThat(actual).isEqualTo("getValue received argument: default");
        }
    }

    @Test
    public void shouldInvokeProxiedMethod() {

        String actual = proxy.getValue("passedThroughByProxy");

        assertThat(actual).isEqualTo("getValue received argument: passedThroughByProxy");

    }

    @Test
    public void shouldNotFailWhenCallingObjectEquals() {
        AnInterface proxy2 = createProxy(AnInterface.class, invocationHandler);
        boolean actual = proxy.equals(proxy2);
        assertThat(actual).isTrue();
    }

    @Test
    public void shouldBeEqualWithItself() {
        @SuppressWarnings("EqualsWithItself")
        boolean actual = proxy.equals(proxy);
        assertThat(actual).isTrue();
    }

    @Test
    public void shouldNotFailWhenCallingEqualsWithNullValue() {
        @SuppressWarnings("ConstantConditions")
        boolean actual = proxy.equals(null);
        assertThat(actual).isFalse();
    }

    @Test
    public void shouldNotBeEqualWithSameProxiedClassAndDifferentInvocationHandler() {
        AnInterface proxy2 = createProxy(AnInterface.class, (c, m, a) -> null);
        boolean actual = proxy.equals(proxy2);
        assertThat(actual).isFalse();
    }

    @Test
    public void shouldBeEqualWithSameProxiedClassAndSameInvocationHandler() {
        AnInterface proxy2 = createProxy(AnInterface.class, invocationHandler);
        boolean actual = proxy.equals(proxy2);
        assertThat(actual).isTrue();
    }

    @Test
    public void shouldNotBeEqualWithDifferentProxiedClassAndSameInvocationHandler() {
        AnotherInterface proxy2 = createProxy(AnotherInterface.class, invocationHandler);
        @SuppressWarnings("EqualsBetweenInconvertibleTypes")
        boolean actual = proxy.equals(proxy2);
        assertThat(actual).isFalse();
    }

    @Test
    public void shouldNotBeEqualWithDifferentProxiedClassAndDifferentInvocationHandler() {
        AnotherInterface proxy2 = createProxy(AnotherInterface.class, (c, m, a) -> null);
        @SuppressWarnings("EqualsBetweenInconvertibleTypes")
        boolean actual = proxy.equals(proxy2);
        assertThat(actual).isFalse();
    }

    @Test
    public void shouldNotBeEqualWithProxiedClass() {
        @SuppressWarnings("EqualsBetweenInconvertibleTypes")
        boolean actual = proxy.equals(AnInterface.class);
        assertThat(actual).isFalse();
    }

    @Test
    public void shouldNotFailWhenCallingObjectHashCode() {
        assertThat(proxy.hashCode()).isNotZero();
    }

    @Test
    public void shouldNotFailWhenCallingObjectToString() {
        assertThat(proxy.toString()).contains("AnInterface");
    }

    @Test
    public void shouldNotFailWhenCallingObjectNotify() {
        synchronized (proxy) {
            assertThatNoException().isThrownBy(proxy::notify);
        }
    }

    @Test
    public void shouldNotFailWhenCallingObjectNotifyAll() {
        synchronized (proxy) {
            assertThatNoException().isThrownBy(proxy::notifyAll);
        }
    }

    @Test(timeout = 2_000)
    public void shouldNotFailWhenCallingObjectWait() {
        synchronizedDelayInNewThread(proxy);

        synchronized (proxy) {
            assertThatNoException().isThrownBy(proxy::wait);
        }
    }

    @Test(timeout = 2_000)
    public void shouldNotFailWhenCallingObjectWaitOneParam() {
        synchronizedDelayInNewThread(proxy);

        synchronized (proxy) {
            assertThatNoException().isThrownBy(() -> proxy.wait(1000));
        }
    }

    @Test(timeout = 2_000)
    public void shouldNotFailWhenCallingObjectWaitTwoParams() {
        synchronizedDelayInNewThread(proxy);

        synchronized (proxy) {
            assertThatNoException().isThrownBy(() -> proxy.wait(1000, 10));
        }
    }

    @Test
    public void shouldConsiderProxyInstanceToBeInstanceOfProxyInterface() {
        assertThat(proxy).isInstanceOf(AnInterface.class);
    }

    @SuppressWarnings("java:S2925")
    private void synchronizedDelayInNewThread(Object o) {
        new Thread(() -> {
            synchronized (o) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException ignored) {
                }
                o.notifyAll();
            }
        }).start();
    }

}
