package org.codefetti.proxy.handler;

public interface AnInterface {

    String getValue(String value);

    default String getDefaultValue() {
        return getValue("default");
    }

}
