package org.codefetti.proxy.handler;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ProvideSecurityManager;
import org.junit.contrib.java.lang.system.internal.NoExitSecurityManager;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.ReflectPermission;
import java.security.AccessControlException;
import java.security.Permission;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assume.assumeTrue;

public class ProxyHandlerWithSecurityManagerJava8Test {

    @Rule
    public final ProvideSecurityManager provideSecurityManager = isJava8()
            ? new ProvideSecurityManager(new NoExitSecurityManager(System.getSecurityManager()) {
                    @Override
                    public void checkPermission(Permission perm) {
                        if (new ReflectPermission("suppressAccessChecks").equals(perm)) {
                            throw new AccessControlException("Not allowed");
                        }
                        super.checkPermission(perm);
                    }
                })
            : new ProvideSecurityManager(System.getSecurityManager()) {
                    @Override protected void before() { /* noop */ }
                    @Override protected void after() { /* noop */ }
                };

    @SuppressWarnings("SuspiciousInvocationHandlerImplementation")
    private final InvocationHandler invocationHandler = (p, m, a) -> "method invoked";

    @Test
    public void shouldFailAtRuntimeWhenInvokingDefaultMethods() {
        onlyJava8();
        AnInterface proxy = InterfaceProxyBuilder.createProxy(AnInterface.class, invocationHandler);

        assertThatExceptionOfType(UnsupportedOperationException.class)
                .isThrownBy(proxy::getDefaultValue)
                .withMessageContaining("security")
                .withMessageContaining("setAccessible")
                .withMessageContaining("default");
    }

    @Test
    public void shouldAnswerNonDefaultInterfaceMethods() {
        onlyJava8();
        AnInterface proxy = InterfaceProxyBuilder.createProxy(AnInterface.class, invocationHandler);

        String value = proxy.getValue("dummy");

        assertThat(value).isEqualTo("method invoked");
    }

    private void onlyJava8() {
        assumeTrue(
                "This test only applies to Java 8. With a SecurityManager preventing " +
                        "setAccessible, Java 9 on later will always fail when the Proxy is " +
                        "created.",
                isJava8());
    }

    private static boolean isJava8() {
        return Double.parseDouble(System.getProperty("java.specification.version")) < 1.9D;
    }

}
