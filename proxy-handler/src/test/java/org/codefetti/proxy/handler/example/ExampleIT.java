package org.codefetti.proxy.handler.example;

import org.codefetti.proxy.handler.InterfaceProxyBuilder;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ExampleIT {

    // interface must be public to grant access to an InvocationHandler in another package
    public interface MyService {
        default String doSomething(String... in) {
            return doSomething(Arrays.asList(in));
        }
        String doSomething(List<String> in);
    }

    static class MyInvocationHandler implements InvocationHandler {
        @Override
        public Object invoke(
                Object proxy,
                @SuppressWarnings("SuspiciousInvocationHandlerImplementation") Method method,
                Object[] args) {
            // Check here if method defines exactly one List<String> argument
            // and return type is String or implement complete dynamic magic
            // based on the method declaration.
            @SuppressWarnings("unchecked")
            List<String> argZeroAsList = (List<String>) args[0];
            return String.join(" ", argZeroAsList);
        }
    }

    private MyService createProxy() {
        return InterfaceProxyBuilder.createProxy(MyService.class, new MyInvocationHandler());
    }

    @Test
    public void shouldBeAbleToCallDefaultMethod() {
        String actual = createProxy().doSomething("Hello", "World");
        assertEquals("Hello World", actual);
    }

    @Test
    public void shouldHandleInvokedMethod() {
        String actual = createProxy().doSomething(Arrays.asList("Hello", "World"));
        assertEquals("Hello World", actual);
    }

    @Test
    public void shouldHandleEqualsMethod() {
        MyInvocationHandler handler = new MyInvocationHandler();
        MyService myServiceOne = InterfaceProxyBuilder.createProxy(MyService.class, handler);
        MyService myServiceTwo = InterfaceProxyBuilder.createProxy(MyService.class, handler);
        MyService myServiceThree = InterfaceProxyBuilder.createProxy(
                MyService.class, new MyInvocationHandler());

        assertEquals(myServiceOne, myServiceTwo);
        assertNotEquals(myServiceOne, myServiceThree);
    }

    @Test
    public void shouldHandleToStringMethod() {
        String actual = createProxy().toString();

        assertEquals("Proxy(MyService)", actual);
    }
}
