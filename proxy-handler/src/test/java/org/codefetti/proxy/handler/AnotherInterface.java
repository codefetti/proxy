package org.codefetti.proxy.handler;

public interface AnotherInterface {

    boolean TRUE = true;
    boolean FALSE = false;

    boolean getBoolean(boolean b);

    @SuppressWarnings("unused")
    default boolean getTrue() {
        return getBoolean(TRUE);
    }

    @SuppressWarnings("unused")
    default boolean getFalse() {
        return getBoolean(FALSE);
    }

}
