package org.codefetti.proxy.handler;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;

import static java.lang.Double.parseDouble;
import static java.lang.System.getProperty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

@SuppressWarnings("SuspiciousInvocationHandlerImplementation")
public class DefaultMethodInvocationHandlerTest {

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    private static final Double USED_RUNTIME_JDK =
            parseDouble(getProperty("java.specification.version"));

    @Test
    public void shouldUseJdk8Handler() {
        assumeThat(USED_RUNTIME_JDK)
                .describedAs("Can not test Java 8 in %s. "
                                     + "Constructor MethodHandles.Lookup(Class, int) used in "
                                     + "DefaultMethodInvocationHandlerJdk8 has been removed from "
                                     + "JDK 14.",
                             USED_RUNTIME_JDK)
                .isLessThan(14);

        System.setProperty("java.specification.version", "1.8");
        DefaultMethodInvocationHandler invocationHandler =
                new DefaultMethodInvocationHandler((p, m, a) -> null);

        assertThat(invocationHandler).extracting("delegate")
                .extracting(o -> o.getClass().getName())
                .isEqualTo(DefaultMethodInvocationHandlerJdk8.class.getName());
    }

    @Test
    public void shouldUseJdk9PlusHandler() {
        System.setProperty("java.specification.version", "11");
        DefaultMethodInvocationHandler invocationHandler =
                new DefaultMethodInvocationHandler((p, m, a) -> null);

        assertThat(invocationHandler).extracting("delegate")
                .extracting(o -> o.getClass().getName())
                .isEqualTo(DefaultMethodInvocationHandlerJdk9Plus.class.getName());
    }

}
