package org.codefetti.proxy.handler;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class ObjectMethodInvocationHandlerTest {

    @Test
    public void shouldFindObjectMethod() throws NoSuchMethodException {
        assertThat(ObjectMethodInvocationHandler.getObjectMethod("hashCode"))
                .isEqualTo(Object.class.getMethod("hashCode"));

    }

    @Test
    public void shouldFindObjectMethodWithParameter() throws NoSuchMethodException {
        assertThat(ObjectMethodInvocationHandler.getObjectMethod("equals", Object.class))
                .isEqualTo(Object.class.getMethod("equals", Object.class));

    }

    @Test
    public void shouldFailWithRuntimeExceptionForUnknownMethod() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> ObjectMethodInvocationHandler.getObjectMethod("foo"));

    }
}
