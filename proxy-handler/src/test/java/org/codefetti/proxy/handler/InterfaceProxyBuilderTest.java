package org.codefetti.proxy.handler;

import org.junit.Test;

public class InterfaceProxyBuilderTest {

    @SuppressWarnings("SuspiciousInvocationHandlerImplementation")
    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForClasses() {
        InterfaceProxyBuilder.createProxy(InterfaceProxyBuilderTest.class, (p, m, a) -> null);
    }
}
