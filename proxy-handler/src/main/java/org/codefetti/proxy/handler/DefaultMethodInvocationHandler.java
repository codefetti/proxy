package org.codefetti.proxy.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * An {@link InvocationHandler} that invokes the real implementation of {@code default} methods
 * declared in interfaces. If the invoked method is not a {@code default} method, a configurable
 * delegate {@link InvocationHandler} is used.
 */
public class DefaultMethodInvocationHandler implements InvocationHandler {

    private final InvocationHandler delegate;

    /**
     * @param delegate the {@link InvocationHandler} to use, if the invoked method is not a default
     *      method
     */
    public DefaultMethodInvocationHandler(InvocationHandler delegate) {
        double javaVersion = Double.parseDouble(System.getProperty("java.specification.version"));
        if (javaVersion < 1.9) {
            this.delegate = new DefaultMethodInvocationHandlerJdk8(delegate);
        }
        else {
            this.delegate = new DefaultMethodInvocationHandlerJdk9Plus(delegate);
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return this.delegate.invoke(proxy, method, args);
    }
}
