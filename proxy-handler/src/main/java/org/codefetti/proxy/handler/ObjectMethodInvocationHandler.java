package org.codefetti.proxy.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * An {@link InvocationHandler} for proxies that handles methods declared in {@link Object}. All
 * other methods are delegated. All proxy instances that implement the same interface and
 * are handled by equal delegate {@code InvocationHandler}s produce the same hash code and are
 * considered as {@link Object#equals(Object) equal}.
 */
public class ObjectMethodInvocationHandler implements InvocationHandler {

    private static final Method TO_STRING =  getObjectMethod("toString");
    private static final Method EQUALS =  getObjectMethod("equals", Object.class);
    private static final Method HASH_CODE =  getObjectMethod("hashCode");

    private final InvocationHandler delegate;

    private final Class<?> proxyInterface;

    /**
     *
     * @param proxyInterface the interface that is proxied
     * @param delegate the invocation handler to use for unhandled methods
     */
    public ObjectMethodInvocationHandler(Class<?> proxyInterface, InvocationHandler delegate) {
        this.delegate = delegate;
        this.proxyInterface = proxyInterface;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (Object.class.equals(method.getDeclaringClass())) {

            if (TO_STRING.equals(method)) {
                return "Proxy(" + proxyInterface.getSimpleName() + ")";
            }

            if (EQUALS.equals(method)) {
                return args[0] != null && (proxy == args[0] || proxy.hashCode() == args[0].hashCode());
            }

            if (HASH_CODE.equals(method)) {
                return Objects.hash(proxyInterface, delegate);
            }
        }
        return delegate.invoke(proxy, method, args);
    }

    static Method getObjectMethod(String name, Class<?>... types) {
        try {
            return Object.class.getMethod(name, types);
        }
        catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Failed to find method " + name + " in Object.", e);
        }
    }

}
