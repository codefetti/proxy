package org.codefetti.proxy.handler;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An {@link InvocationHandler} that invokes the real implementation of {@code default} methods
 * declared in interfaces. If the invoked method is not a {@code default} method, a configurable
 * delegate {@link InvocationHandler} is used.
 */
class DefaultMethodInvocationHandlerJdk9Plus implements InvocationHandler {

    private final InvocationHandler delegate;

    /**
     * Caches all {@link MethodHandle} instances by the hash of the proxy and the method.
     */
    private final Map<Integer, MethodHandle> methodHandleCache = new ConcurrentHashMap<>();

    /**
     * @param delegate the {@link InvocationHandler} to use, if the invoked method is not a default
     *      method
     */
    DefaultMethodInvocationHandlerJdk9Plus(InvocationHandler delegate) {
        this.delegate = delegate;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (!method.isDefault()) {
            return delegate.invoke(proxy, method, args);
        }

        int defaultMethodHash = createHash(proxy, method);
        MethodHandle methodHandle = methodHandleCache.get(defaultMethodHash);
        if (methodHandle != null) {
            return methodHandle.invokeWithArguments(args);
        }

        return invokeJdk9Plus(defaultMethodHash, proxy, method, args);

    }

    /**
     * JDK9 and above allow access by setting the specialCaller to the same value as the reference
     * class. There is no need to invoke the private constructor. There is also no way to invoke it
     * because it can't be set accessible in newer JDK versions.
     *
     * @see <a href="http://mail.openjdk.java.net/pipermail/jigsaw-dev/2017-January/010741.html">OpenJDK Mailing List</a>
     */
    private Object invokeJdk9Plus(int methodHash, Object proxy, Method method, Object[] args)
            throws Throwable {
        Class<?> declaringClass = method.getDeclaringClass();
        MethodHandle methodHandle = MethodHandles.lookup().findSpecial(
                declaringClass,
                method.getName(),
                MethodType.methodType(method.getReturnType(), method.getParameterTypes()),
                declaringClass);
        MethodHandle boundMethodHandle = methodHandle.bindTo(proxy);
        methodHandleCache.put(methodHash, boundMethodHandle);
        return boundMethodHandle.invokeWithArguments(args);
    }

    private int createHash(Object proxy, Method method) {
        return Objects.hash(
                proxy,
                method.getDeclaringClass(),
                method.getReturnType(),
                method.getName(),
                Objects.hash((Object[]) method.getParameterTypes())
        );
    }

}
