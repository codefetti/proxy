package org.codefetti.proxy.handler;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An {@link InvocationHandler} for JDK 8 that invokes the real implementation of {@code default}
 * methods declared in interfaces. If the invoked method is not a {@code default} method, a
 * configurable delegate {@link InvocationHandler} is used.
 */
class DefaultMethodInvocationHandlerJdk8 implements InvocationHandler {

    private final InvocationHandler delegate;

    /**
     * Caches all {@link MethodHandle} instances by the hash of the proxy and the method.
     */
    private final Map<Integer, MethodHandle> methodHandleCache = new ConcurrentHashMap<>();

    /*
     * Used for JDK8 to create {@link MethodHandles.Lookup} with private access privileges.
     *
     * @see <a href="https://stackoverflow.com/a/26211382">Stackoverflow: Java8 dynamic proxy and default methods</a>
     */
    private final Constructor<MethodHandles.Lookup> lookupConstructor;

    /**
     * @param delegate the {@link InvocationHandler} to use, if the invoked method is not a default
     *      method
     */
    DefaultMethodInvocationHandlerJdk8(InvocationHandler delegate) {
        this.delegate = delegate;
        this.lookupConstructor = initLookupConstructorForJdk8();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (!method.isDefault()) {
            return delegate.invoke(proxy, method, args);
        }

        int defaultMethodHash = createHash(proxy, method);
        MethodHandle methodHandle = methodHandleCache.get(defaultMethodHash);
        if (methodHandle != null) {
            return methodHandle.invokeWithArguments(args);
        }

        if (this.lookupConstructor != null) {
            return invokeJdk8(defaultMethodHash, proxy, method, args);
        }
        else {
            throw new UnsupportedOperationException("Failed to invoke default method "
                    + method + ". Maybe there are security restrictions in the JVM that "
                    + "prevent using setAccessible(true) for "
                    + "MethodHandles.Lookup(Class<?>, int). In this environment proxied "
                    + "interfaces can't declare default methods.");
        }

    }

    /**
     * Uses the private {@link #lookupConstructor} of {@link MethodHandles.Lookup} to create a
     * {@link MethodHandle} for the default method to invoke.
     *
     * @see <a href="https://stackoverflow.com/a/26211382">Stackoverflow: Java8 dynamic proxy and default methods</a>
     */
    private Object invokeJdk8(int methodHash, Object proxy, Method method, Object[] args)
            throws Throwable {
        Class<?> declaringClass = method.getDeclaringClass();
        MethodHandles.Lookup lookup = this.lookupConstructor
                .newInstance(declaringClass, MethodHandles.Lookup.PRIVATE);
        MethodHandles.Lookup in = lookup.in(declaringClass);
        MethodHandle methodHandle = in.unreflectSpecial(method, declaringClass);
        MethodHandle boundMethodHandle = methodHandle.bindTo(proxy);
        methodHandleCache.put(methodHash, boundMethodHandle);
        return boundMethodHandle.invokeWithArguments(args);
    }

    private int createHash(Object proxy, Method method) {
        return Objects.hash(
                proxy,
                method.getDeclaringClass(),
                method.getReturnType(),
                method.getName(),
                Objects.hash((Object[]) method.getParameterTypes())
        );
    }

    @SuppressWarnings("java:S3011") // setting accessibility is the only option here
    private Constructor<MethodHandles.Lookup> initLookupConstructorForJdk8() {
        try {
            Constructor<MethodHandles.Lookup> declaredConstructor = MethodHandles.Lookup.class
                    .getDeclaredConstructor(Class.class, int.class);
            declaredConstructor.setAccessible(true);
            return declaredConstructor;
        }
        catch (NoSuchMethodException e) {
            throw new IllegalAccessError("Unexpected Api: "
                    + "MethodHandles.Lookup has not the expected Constructor "
                    + "MethodHandles.Lookup(Class, int)"
            );
        }
        catch (SecurityException ignored) {
            // Ignore the SecurityException.
            // If nobody declares or invokes default methods, there will be no problem.
            // If such method is invoked in this environment it will cause an
            // UnsupportedOperationException at runtime.
            return null;
        }
    }
}
