package org.codefetti.proxy.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Creates instances of interface types using {@link Proxy}.
 */
public class InterfaceProxyBuilder {

    private InterfaceProxyBuilder() {
        // a utility class
    }

    /**
     *
     * @param interfaceType interface type that should be implemented as proxy
     * @param invocationHandler the invocation handler that should handle invoked methods that are
     *      not {@code default} interface implementations and not methods declared in {@link Object}
     * @param <I> the type of the proxy to implement
     * @return the interface proxy instance
     */
    public static <I> I createProxy(Class<I> interfaceType, InvocationHandler invocationHandler) {

        if (!interfaceType.isInterface()) {
            throw new IllegalArgumentException(
                    "Only interfaces can be proxied with the " + InterfaceProxyBuilder.class);
        }

        Object proxyInstance = Proxy.newProxyInstance(
                InterfaceProxyBuilder.class.getClassLoader(),
                new Class[] { interfaceType },
                new DefaultMethodInvocationHandler(
                        new ObjectMethodInvocationHandler(interfaceType, invocationHandler)
                )
        );

        @SuppressWarnings("unchecked")
        I instance = (I) proxyInstance;
        return instance;

    }


}
