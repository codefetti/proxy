package org.codefetti.proxy.handler.tests;

public interface TheExtendingInterface extends TheInterface {

    default String doSomething(float a, float b) {
        return doSomething("" + a, "" + b);
    }

    default String doSomething(byte a, byte b) {
        return doSomething(a, (int) b);
    }

    default String doSomething(String a, String b) {
        return doSomething(a + " " + b);
    }

    String doSomething(Object o);
}
