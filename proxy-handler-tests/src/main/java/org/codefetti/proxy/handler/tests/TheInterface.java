package org.codefetti.proxy.handler.tests;

public interface TheInterface {

    default String doSomething(int a, int b) {
        return doSomething((float) a, (float) b);
    }

    String doSomething(float a, float b);

}
