package org.codefetti.proxy.handler.tests;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;

import static org.codefetti.proxy.handler.InterfaceProxyBuilder.createProxy;

@SuppressWarnings("SuspiciousInvocationHandlerImplementation")
public class InterfaceTest {

    private static final InvocationHandler DEFAULT_INVOCATION_HANDLER = (p, m, a) -> {
        if (m.getName().equals("doSomething")
                && m.getReturnType().equals(String.class)
                && m.getParameterTypes().length == 1
                && m.getParameterTypes()[0].equals(Object.class)
        ) {
            if (a[0] == null) {
                return "";
            }
            else {
                return a[0].toString();
            }
        }
        throw new RuntimeException("Method " + m + " not handled.");
    };

    private final TheExtendingInterface proxy = createProxy(
            TheExtendingInterface.class,
            DEFAULT_INVOCATION_HANDLER);

    @Test
    public void invokeDefaultMethodOnSuperInterface() {
        String actual = proxy.doSomething(1, 2);
        Assert.assertEquals("1.0 2.0", actual);
    }

    @Test
    public void invokeDefaultMethodOnSuperInterfaceTwice() {
        String actualA = proxy.doSomething(1, 2);
        String actualB = proxy.doSomething(1, 2);
        Assert.assertEquals(actualA, actualB);
    }

    @Test
    public void invokeDefaultMethodOnExtendingInterface() {
        String actual = proxy.doSomething(1.5f, 2.5f);
        Assert.assertEquals("1.5 2.5", actual);
    }

    @Test
    public void invokeDefaultMethodOnExtendingInterfaceCallingSuperInterface() {
        String actual = proxy.doSomething((byte) 2, (byte) 3);
        Assert.assertEquals("2.0 3.0", actual);
    }

    @Test
    public void invokeDefaultMethodOnExtendingInterfaceCallingExtendingInterface() {
        String actual = proxy.doSomething("Foo", "Bar");
        Assert.assertEquals("Foo Bar", actual);
    }

    @Test
    public void invokeMethodHandledByProxy() {
        String actual = proxy.doSomething("Foo");
        Assert.assertEquals("Foo", actual);
    }

    @Test
    public void invokeToString() {
        String actual = proxy.toString();
        Assert.assertEquals("Proxy(TheExtendingInterface)", actual);
    }

    @Test
    public void invokeEqualsTrue() {
        TheExtendingInterface anotherProxy = createProxy(
                TheExtendingInterface.class,
                DEFAULT_INVOCATION_HANDLER);
        Assert.assertEquals(proxy, anotherProxy);
        Assert.assertNotSame(proxy, anotherProxy);
        Assert.assertEquals(anotherProxy, proxy);
        Assert.assertNotSame(anotherProxy, proxy);
    }

    @Test
    public void invokeEqualsFalseDifferentHandler() {
        TheExtendingInterface anotherProxy = createProxy(
                TheExtendingInterface.class,
                (p, m, a) -> null);
        Assert.assertNotEquals(proxy, anotherProxy);
        Assert.assertNotSame(proxy, anotherProxy);
        Assert.assertNotEquals(anotherProxy, proxy);
        Assert.assertNotSame(anotherProxy, proxy);
    }

    @Test
    public void invokeEqualsFalseDifferentProxy() {
        InvocationHandler invocationHandler = (p, m, a) -> null;
        TheExtendingInterface proxyA = createProxy(
                TheExtendingInterface.class,
                invocationHandler
        );
        TheInterface proxyB = createProxy(
                TheInterface.class,
                invocationHandler
        );
        Assert.assertNotEquals(proxyA, proxyB);
        Assert.assertNotSame(proxyA, proxyB);
        Assert.assertNotEquals(proxyB, proxyA);
        Assert.assertNotSame(proxyB, proxyA);
    }

    @Test
    public void invokeHashCodeTrue() {
        TheExtendingInterface anotherProxy = createProxy(
                TheExtendingInterface.class,
                DEFAULT_INVOCATION_HANDLER);
        Assert.assertEquals(proxy.hashCode(), anotherProxy.hashCode());
    }

    @Test
    public void invokeHashCodeFalseDifferentHandler() {
        TheExtendingInterface anotherProxy = createProxy(
                TheExtendingInterface.class,
                (p, m, a) -> null);
        Assert.assertNotEquals(proxy.hashCode(), anotherProxy.hashCode());
    }

    @Test
    public void invokeHashCodeFalseDifferentProxy() {
        InvocationHandler invocationHandler = (p, m, a) -> null;
        TheExtendingInterface proxyA = createProxy(
                TheExtendingInterface.class,
                invocationHandler
        );
        TheInterface proxyB = createProxy(
                TheInterface.class,
                invocationHandler
        );
        Assert.assertNotEquals(proxyA.hashCode(), proxyB.hashCode());
    }
}
