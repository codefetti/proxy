# codefetti.org Proxy

A Java utility to work with Proxies and InvocationHandlers. codefetti.org Proxy is tested with 
Java 8, Java 11, Java 13, Java 14, Java 15 and Java 17.

All changes are documented in the [CHANGELOG](./CHANGELOG.md).


## Modules

### proxy-handler

The `proxy-handler` module provides `InvocationHandler` implementations that care about `default`
methods in interfaces and invocations of methods from `Object` like `toString()`, `equals(Object)`
and `hashCode()`.

When a `Proxy` does not handle `toString()` properly for example it causes unexpected behaviour 
when debugging or logging. 

Also, the helpers in `proxy-handler` invoke the real implementation of `default` methods. The 
custom `InvocationHandler` only has to care about the declared interface methods that have no 
implementation.


#### Usage

First add the dependency to your project.

Maven:
```xml
<dependency>
  <group-id>org.codefetti.proxy</group-id>
  <artifact-id>proxy-handler</artifact>
  <version>[latest-version]</version>
</dependency>
```

Gradle:
```groovy
dependencies {
  compile "org.codefetti.proxy:proxy-handler:[latest-version]"
}
```

Second create your `Proxy` instance using
[`InterfaceProxyBuilder.createProxy(Class<I>, InvocationHandler)`](./proxy-handler/src/main/java/org/codefetti/proxy/handler/InterfaceProxyBuilder.java)


#### Example

The following example is covered in the 
[ExampleIT](./proxy-handler/src/test/java/org/codefetti/proxy/handler/example/ExampleIT.java).

Create an interface:
```java
public interface MyService {
    default String doSomething(String... in) {
        return doSomething(Arrays.asList(in));
    }
    String doSomething(List<String> in);
}
```

Create an `InvocationHandler` that handles the non default methods:
```java
class MyInvocationHandler implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        // check here if method defines exactly one List<String> argument 
        // and return type is String
        return String.join(" ", (List<String>) args[0]);
    }
}
```

Get an instance for the interface:
```java
class Init {
    MyService createProxy() {
        return InterfaceProxyBuilder.createProxy(MyService.class, new MyInvocationHandler());
    }
}
```

In real scenarios the custom `InvocationHandler` would of course do more checks on the method,
interpret annotations and do some magic to create logic dynamically for the interface definition.

As shown in the example, the custom `InvocationHandler` must not care about the default method and
the methods inherited from `Object`.
