---
# This Yaml header defines the version bump as major, minor or patch.
bump: patch
---
# Changelog

All notable changes to Codefetti proxy will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased

No notable changes yet.


## [1.0.3](https://gitlab.com/codefetti/proxy/compare/1.0.2...1.0.3) - 2023-09-09

### Added

- Verify support for Java 19
- Verify support for Java 21

### Removed

- Testing of non-LTS Java versions 13, 14 and 15 has been removed. Proxy Handler will most likely
  still work with these version. Upgrading to a maintained LTS Java version should be considered.


## [1.0.2](https://gitlab.com/codefetti/proxy/compare/1.0.1...1.0.2) - 2021-12-16

### Fixed

- Resolved code style warnings. Behavior should not be affected.


## [1.0.1](https://gitlab.com/codefetti/proxy/compare/1.0.0...1.0.1) - 2021-09-30

### Removed

- JDK 9 is not tested anymore. Proxy Handler will most likely still work with Java 9 but due to
  Gradle upgrades the build broke and fixing it is not valuable for an EOL version.

### Added

- Verify support for Java 14
- Verify support for Java 15
- Verify support for Java 17


## [1.0.0](https://gitlab.com/codefetti/proxy/compare/0.0.8...1.0.0) - 2019-12-18

### Added

- The initial release adds `InvocationHandler` implementations that call default methods of 
  interfaces and methods declared by `Object`.
- An example has been added as integration test and is referenced in the README.
